# Working with Blueocean
---

## Step By Step Instructions

### Create the Pipeline

- Open the Blue Ocean Console:
 
![Image](images/solution-01.png)

- Click on "New Pipeline" to create the pipeline:

![Image](images/solution-02.png)

- Select "Git" as source code location:

![Image](images/solution-03.png)

- Set your repository url (https) and set your gitlab credentials, then click on "create credential":

![Image](images/solution-04.png)

- Finally click on "Create Pipeline" (if it's stuck, just refresh the page and look for the created pipeline)

![Image](images/solution-05.png)


### Configure the Pipeline

- In the general pipeline settings configure your pipeline to run in as default in the "Slave" node

![Image](images/solution-06.png)

- For the "Restore" stage configure a single "shell script" step to run "npm install"

![Image](images/solution-07.png)

- For the "Test" configure 3 parallel stages with single "shell script" steps to run the tests "npm run test1", "npm run test2" and "npm run test3"

![Image](images/solution-08.png)

- For the "Test Env" stage configure a single "shell script" step to run "docker-compose up -d --build" and configure it to run on the "Test" agent.

![Image](images/solution-09.png)

![Image](images/solution-10.png)

- For the "Approval" stage configure a single "Wait for interactive input" step with the message "Deploy to Production?"

![Image](images/solution-11.png)

- For the "Prod Env" stage configure a single "shell script" step to run "docker-compose up -d --build" and configure it to run on the "Production" agent.

![Image](images/solution-12.png)

![Image](images/solution-13.png)

- Finally click save and commit the changes

- In the repository you can see the created Jenkinsfile

![Image](images/solution-14.png)