# Working with Jenkins Blue Ocean
---

### Prerequisites

- Server's ips
- Gitlab account

### Import the lab repository

 - Browse to the Gitlab server, login and create a new project
 
![Image 1](images/image-01.png)


 - Select import repository, repo by url and paste the url below
 
```
https://gitlab.com/sela-devops/courses/jenkins-cicd/demo-app.git
```

![Image 2](images/image-02.png)

 - Set the project name
 
```
demo-app-blue
```

![Image 3](images/image-03.png)

 - Set the visibility level as "Public"
 
![Image 4](images/image-04.png)

---

## Meeting the Application

![Image](images/application.png)

- To restore the project dependencies you can use:

```
npm install
```

- To test the application you can use the following commands to run the tests:

```
npm run test1
```
```
npm run test2
```
```
npm run test3
```

- To deploy the application you can use docker compose by running the following command:

```
docker-compose up -d
```


## The Challenge

 - In this lab we want to create a pipeline using Jenkins Blue ocean with the following steps:
   1. Restore project dependencies
   2. Run tests (in parallel)
   3. Deploy to Test (Continuous Deployment)
   4. Wait for user approval
   5. Deploy to Production (Continuous Delivery)

 - It should look as shown below:

![Image](images/pipeline.png)

 - Try to do it by yourself, however if you want the step by step solution see [Solution.md](./Solution.md)


## Configure the Triggers

 - To trigger the pipeline after each change (Continuous Integration) we will need the Gitlab plugin, let's install it:

![Image](images/plugin.png)

 - Then we can configure Gitlab to trigger the Jenkins Pipeline after any change:

![Image](images/trigger.png)    


## Test Your CI/CD Process

 - Now that everything is ready, let's change the title of the web page to test that everything works as expected. Edit the index.html file and replace the title which is on line 218

![Image](images/test.png)

 - Commit your changes and browse to your Jenkins dashboard to track the process
